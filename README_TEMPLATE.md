# PKGNAME

This project was generated using [hs-template](https://gitlab.com/joncol/hs-template).

## Building and running

To build and run the project, use:
```bash
nix run .#PKGNAME:exe:PKGNAME-exe
```

To only build the project, use:
```bash
nix build
```

To show all Flake outputs, you can use:
```bash
nix flake show --option allow-import-from-derivation true
```
